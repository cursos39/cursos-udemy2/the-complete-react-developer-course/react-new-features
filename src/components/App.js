import React, { useState, useEffect } from 'react';

const App = (props) => {
  const [count, setCount] = useState(props.count)
  const [text, setText] = useState('')

  // It executes only one time (for getting data) --> componentDidMount
  useEffect(() => {
    console.log('This should only run once')
  }, []) 

  // It executes only when count changes
  useEffect(() => {
    console.log('useEffect ran')
    document.title = count
  }, [count])

  const increment = () => setCount(count +1)
  const reset = () => setCount(props.count)
  const decrement = () => setCount(count -1)
  const onChangeText = e => setText(e.target.value)

  return (
    <div>
      <p>The current {text || 'count'} is {count}</p>
      <button onClick={increment}>+1</button>
      <button onClick={reset}>Reset</button>
      <button onClick={decrement}>-1</button>
      <input type='text' value={text} onChange={onChangeText}/>
    </div>
  )
}

App.defaultProps = {
  count: 0
}

export default App