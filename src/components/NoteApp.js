import React, { useEffect, useReducer } from 'react';
import notesReducer from '../reducers/notes';
import AddNoteForm from './AddNoteForm';
import NoteList from './NoteList';
import NotesContext from '../context/notes-context';


const NoteApp = () => {
  const [notes, dispatch] = useReducer(notesReducer, [])

  useEffect(() => {
    const notesStoraged = localStorage.getItem('notes')
    if (notesStoraged) {
      dispatch({type: 'POPULATE_NOTES', notes: JSON.parse(notesStoraged)})  
    }
  }, [])

  useEffect(() => {
    localStorage.setItem('notes', JSON.stringify(notes))
  }, [notes])

  return (
    <NotesContext.Provider value={{ notes, dispatch }}>
      <h1>Notes</h1>
      <NoteList />
      <AddNoteForm />
    </NotesContext.Provider>
    
  )
}

export default NoteApp