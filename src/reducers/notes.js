const notesReducer = (notes, action) => {
  switch(action.type){
    case 'ADD_NOTE':
      return [...notes, action.note]
    case 'REMOVE_NOTE':
      return notes.filter(note => note.title !== action.title)
    case 'POPULATE_NOTES':
      return action.notes
    default:
      return notes
  }
}

export { notesReducer as default }